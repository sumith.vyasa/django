from django.urls import path
from .views import blog_list,blog_detail,blog_delete

from . import views

urlpatterns = [
    # ex: /polls/
    path('', blog_list),
    path('<id>/', blog_detail),
    path('<id>/delete/', blog_delete)
]
